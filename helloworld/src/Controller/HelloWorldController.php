<?php

namespace Drupal\helloworld\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for page example routes.
 */
class HelloWorldController extends ControllerBase {

  /**
   * Constructs a page with descriptive content.
   *
   * Our router maps this method to the path 'examples/page_example'.
   */
  public function hello() {
    $build = array();

    $build['#title'] = 'Hello world title';
    $build['#markup'] = 'Hello World!';

    return $build;
  }
}
