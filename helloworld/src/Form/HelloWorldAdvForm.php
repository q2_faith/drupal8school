<?php
/**
 * @file
 * Contains Drupal\helloworld\Form\HelloWorldAdvForm.
 */

namespace Drupal\helloworld\Form;


use Drupal\Core\Form\FormStateInterface;
use Drupal\helloworld\HelloWorldEvent;
use Drupal\helloworld\Event\HelloWorldEvents;

class HelloWorldAdvForm extends HelloWorldForm{

  /**
   * {@inheritdoc}
   *
   */
  public function getFormId() {
    return 'helloworldadv_form';
  }

  /**
   * @param array $form
   * @param \Drupal\helloworld\Form\FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $tel = $form_state->getValue('tel');

    $dispatcher = \Drupal::service('event_dispatcher');
    $event = new HelloWorldEvent($tel);
    $dispatcher->dispatch(HelloWorldEvents::HelloWorldSubmit, $event);
    if ($event) {
      drupal_set_message($this->t('Someone changed @tel', array('@tel' => $event->getTel($tel))));
    }
    else {
      drupal_set_message($this->t('Your phone number @tel.', array('@tel' => $tel)));
    }
  }
}
