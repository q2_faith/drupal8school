<?php
/**
 * @file
 * Contains Drupal\helloworld\HelloWorldEvent.
 */

namespace Drupal\helloworld;


use Symfony\Component\EventDispatcher\Event;

class HelloWorldEvent extends Event{

  protected $tel;

  public function __construct($tel) {
    $this->tel = $tel;
  }

  public function getTel($tel) {
    return $this->tel = $tel;
  }
}
