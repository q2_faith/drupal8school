<?php

namespace Drupal\currencies\Plugin\Block;


use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Currencies' Block
 *
 * @Block(
 *   id = "currencies_block",
 *   admin_label = @Translation("Currencies block"),
 * )
 */
class CurrenciesBlock extends BlockBase{

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * If a block should not be rendered because it has no content, then this
   * method must also ensure to return no content: it must then only return an
   * empty array, or an empty array with #cache set (with cacheability metadata
   * indicating the circumstances for it being empty).
   *
   * @return array
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build() {
    $rows = array();
    $service = \Drupal::service('currencies.service');
    $currencies_data = $service->getValue();
    $config = \Drupal::config('currencies.settings');

    $header = array(
      $this->t('Alphabetic code'),
      $this->t('The number of foreign currency units'),
      $this->t('Official rate'),
    );

    foreach ($config->get('currencies') as $currency) {
      if ($currency != 0) {
        $rows[] = array(
          $currencies_data[$currency]->Cur_Abbreviation,
          $currencies_data[$currency]->Cur_Scale,
          $currencies_data[$currency]->Cur_OfficialRate,
        );
      }

    }

    $date = \Drupal::service('date.formatter')
      ->format(REQUEST_TIME, $type = 'custom', 'm/d/Y');

    $build['date'] = array(
      '#type' => 'markup',
      '#markup' => $this->t('@date', array('@date' => $date)),
    );

    $build['table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );

    return $build;
  }
}