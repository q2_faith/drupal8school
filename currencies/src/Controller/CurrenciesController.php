<?php

namespace Drupal\currencies\Controller;


use Drupal\Core\Controller\ControllerBase;

class CurrenciesController extends ControllerBase{


  public function content() {
    $rows = array();
    $service = \Drupal::service('currencies.service');

    $header = array(
      $this->t('Alphabetic code'),
      $this->t('The number of foreign currency units'),
      $this->t('Foreign currency name'),
      $this->t('Official rate'),
    );

    foreach ($service->getValue() as $currency) {
      $rows[] = array(
        $currency->Cur_Abbreviation,
        $currency->Cur_Scale,
        $currency->Cur_Name,
        $currency->Cur_OfficialRate,
      );
    }

    $date = \Drupal::service('date.formatter')
      ->format(REQUEST_TIME, $type = 'custom', 'm/d/Y');

    $build['date'] = array(
      '#type' => 'markup',
      '#markup' => $this->t('Official Exchange Rate on @date', array('@date' => $date)),
    );

    $build['table'] = array(
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );

    return $build;
  }}