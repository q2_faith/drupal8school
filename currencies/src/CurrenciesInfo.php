<?php
/**
 * @file
 * Contains Drupal\currencies\CurrenciesInfo.
 */

namespace Drupal\currencies;



use GuzzleHttp\Exception\RequestException;

class CurrenciesInfo {

  protected $value;

  public function __construct() {

    $client = \Drupal::httpClient();

    try {
      $content = array();
      $date = \Drupal::service('date.formatter')
        ->format(REQUEST_TIME, $type = 'custom', 'm/d/Y');
      $request = $client->request('GET', 'http://www.nbrb.by/API/ExRates/Rates', [
        'query' => ['onDate' => $date, 'Periodicity' => 0]
      ]);
      $body = $request->getBody();
      $elements = json_decode($body->getContents());
      foreach ($elements as $element) {
        $content[$element->Cur_ID] = $element;
      }
      $this->value = $content;
    }
    catch (RequestException $e) {
      watchdog_exception('currencies', $e->getMessage());
    }    
  }

  /**
   * @return mixed
   */
  public function getValue() {
    return $this->value;
  }
}