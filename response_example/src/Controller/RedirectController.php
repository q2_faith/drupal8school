<?php

namespace Drupal\response_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class RedirectController.
 *
 * @package Drupal\response_example\Controller
 */
class RedirectController extends ControllerBase {

  /**
   * Redirect.
   *
   * @return string
   *   Return Hello string.
   */
  public function redirectexample() {
    return new RedirectResponse('/');
  }

}
