<?php

namespace Drupal\response_example\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class CustomElement.
 *
 * @package Drupal\response_example\Controller
 */
class CustomElement extends ControllerBase {

  /**
   * Sample.
   *
   * @return string
   *   Return Hello string.
   */
  public function sample() {
    return [
      '#type' => 'custom_element',
      '#label' => $this->t('Default Label'),
      '#description' => $this->t('Default Description'),
    ];
  }

}
