<?php

namespace Drupal\response_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StatusController.
 *
 * @package Drupal\response_example\Controller
 */
class StatusController extends ControllerBase {

  /**
   * Status.
   *
   * @return string
   *   Return Hello string.
   */
  public function status() {

    $response = new Response('Set status code: 404');
    $response->headers->set('Content-Type', 'text/plain');
    $response->setStatusCode(Response::HTTP_NOT_FOUND);
    return $response;
  }

}
