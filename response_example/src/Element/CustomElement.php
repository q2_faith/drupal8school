<?php
/**
 * @file
 * Contains Drupal\response_example\Element\CustomElement.
 */

namespace Drupal\response_example\Element;


use Drupal\Core\Render\Element\RenderElement;

/**
 * Class CustomElement
 * @package Drupal\response_example\Element
 *
 * @RenderElement("custom_element")
 */
class CustomElement extends RenderElement {

  /**
   * Returns the element properties for this element.
   *
   * @return array
   *   An array of element properties. See
   *   \Drupal\Core\Render\ElementInfoManagerInterface::getInfo() for
   *   documentation of the standard properties of all elements, and the
   *   return value format.
   */
  public function getInfo() {
    $class = get_class($this);
    return array(
      '#theme' => 'custom_element',
      '#label' => 'Default Label',
      '#description' => 'Default Description',
      '#pre_render' => array(
        array($class, 'preRenderCustomElement'),
      ),
    );
  }

  public static function preRenderCustomElement($element) {
    $element['label'] = array(
      '#markup' => $element['#label']
    );
    $element['description'] = array(
      '#markup' => $element['#description']
    );

    return $element;
  }
}
