<?php

namespace Drupal\response_example\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the My element plugin manager.
 */
class MyElementManager extends DefaultPluginManager {


  /**
   * Constructor for MyElementManager objects.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/MyElement', $namespaces, $module_handler, 'Drupal\response_example\Plugin\MyElementInterface', 'Drupal\response_example\Annotation\MyElement');

    $this->alterInfo('response_example_my_element_info');
    $this->setCacheBackend($cache_backend, 'response_example_my_element_plugins');
  }

}
