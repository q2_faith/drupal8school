<?php

namespace Drupal\response_example\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for My element plugins.
 */
interface MyElementInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.

}
