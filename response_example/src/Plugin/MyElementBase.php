<?php

namespace Drupal\response_example\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for My element plugins.
 */
abstract class MyElementBase extends PluginBase implements MyElementInterface {


  // Add common methods and abstract methods for your plugin type here.

}
